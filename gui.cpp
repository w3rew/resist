#include <SFML/Graphics.hpp>
#include<iostream>
#include<vector>
#include<utility>
#include<string>
#include "header.h"
using namespace std;
int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "SFML works!");
    sf::CircleShape dot(5.f);
    dot.setFillColor(sf::Color::Green);
    sf::CircleShape start(8.f);
    start.setFillColor(sf::Color::Blue);
    sf::Font font;
    font.loadFromFile("arial.ttf");

    vector<vector<int>> v(100, vector<int>(100, -1));
    int num = 1;
    int offset = 10;
    int out = -1;

    vector<vector<pair<rational, int>>> graph(1);

    double x0 = 30;
    double y0 = 100;
    double stepx = 80.f;
    double stepy = 60.f;
    sf::VertexArray xnet(sf::Lines, 300);
    for(int i = 0; i < 30; i+=2) {
        xnet[i].position = sf::Vector2f(x0+i*stepx/2, 0);
        xnet[i+1].position = sf::Vector2f(x0+i*stepx/2, 1000);
    }
    sf::VertexArray ynet(sf::Lines, 300);
    for(int i = 0; i < 30; i += 2) {
        ynet[i].position = sf::Vector2f(0, y0+i*stepy/2);
        ynet[i+1].position = sf::Vector2f(1000,y0+i*stepy/2);
    }
    sf::VertexArray l(sf::Lines);
    vector<sf::Text> labels;

    int x = 2;
    int y = 7;
    v[x][y] = 0;
    int sink;
    start.setPosition(x0 + stepx*x, y0+stepy*y);
    bool f = false;
    int x1, y1, x2, y2;
    while (window.isOpen())
    {

        sf::Event event;
        while (window.pollEvent(event))
        {
            if(event.type == sf::Event::KeyPressed) {
                switch(event.key.code) {
                    case sf::Keyboard::Left:
                        x --;
                        break;
                    case sf::Keyboard::Right:
                        x ++;
                        break;
                    case sf::Keyboard::Down:
                        y ++;
                        break;
                    case sf::Keyboard::Up:
                        y --;
                        break;
                    case sf::Keyboard::Enter:
                        if (!f){
                            x1 = x;
                            y1 = y;
                            f = true;
                        }
                        else{                
                            x2 = x;
                            y2 = y;
                            auto a = sf::Vertex(sf::Vector2f(x0 + x1*stepx, y0+stepy*y1), sf::Color::Red);
                            l.append(a);
                            a = sf::Vertex(sf::Vector2f(x0+stepx*x2, y0+stepy*y2), sf::Color::Red);
                            l.append(a);
                            if (v[x][y] == -1) {
                                v[x][y] = num;
                                num++;
                                graph.push_back({});
                            }
                            rational r;
                            std::cout<<"Введите сопротивление: ";
                            std::cin >> r;
                            std::cout << std::endl;
                            graph[v[x1][y1]].push_back(make_pair(r, v[x][y]));
                            graph[v[x][y]].push_back(make_pair(r, v[x1][y1]));
                            f = false;
                            sf::Text text;
                            text.setFont(font);
                            text.setString (to_string(r.numerator()) + "/" + to_string(r.denominator()));
                            text.setCharacterSize(20);
                            text.setFillColor(sf::Color::White);
                            text.setPosition((x1+x2)/2*stepx + x0, y0 + stepy*(y1 + y2)/2);
                            labels.push_back(text);
                        }
                        break;

                    case sf::Keyboard::Space:
                        sink = v[x][y];
                        window.close();
                        break;
                }

            }

            if (event.type == sf::Event::Closed)
                window.close();

            dot.setPosition(x0 + x*stepx, y0 + y*stepy);
            window.clear(sf::Color::Black);
            window.draw(ynet);
            window.draw(xnet);
            window.draw(l);
            window.draw(dot);
            window.draw(start);
            for (auto& i : labels) window.draw(i);
            window.display();
        }
    }
    //cout << graph.size() << endl;
    LinearSystem s(graph.size());
    for (int i = 1; i < graph.size(); ++i) {
        vector<rational> line(graph.size() + 1, 0);
        if (i == sink) continue;
        for (auto j : graph[i]) {
            line[i] += rational(1)/j.first;
            line[j.second] += rational(-1)/j.first;
        }
        s.addLine(line);
    }
    vector<rational> line(graph.size() + 1, 0);
    line[0] = 1;
    line[graph.size()] = 1; //PHI
    s.addLine(line);
    line = vector<rational>(graph.size() + 1, 0);
    line[sink] = 1;
    s.addLine(line);
    s.Solve();
    auto sol = s.getSolution();
    //cout << (*sol)[0] << endl;
    rational I = 0;
    for (auto i : graph[0]) {
        I += (rational(1) - (*sol)[i.second])/i.first;
    }
    cout << "Сопротивление " << rational(1)/I << endl;
    delete sol;
    return 0;
}
