#include "header.h"
#include <boost/rational.hpp>
#include <vector>
#include <iostream>
const int IN = -2;
const int OUT = -1;
const rational PHI = rational(1);
using namespace std;

int main() {
    //freopen("t", "r", stdin);
    int n;
    cout << "Количество вершин кроме стока и истока: " << endl;
    cin >> n;
    LinearSystem s(n);
    vector<pair<int, rational>> adj_out;
    for (int i = 0; i < n; ++i) {
        cout << "Вершина(вершины нумеруются с 0, исток " << IN << " , сток " << OUT << "): ";
        int cur;
        cin >> cur;
        cout << "Количество ребер: ";
        int adj;
        cin >> adj;
        cout << "Смежные: номер сопротивление" << endl;
        vector<rational> line(n + 1, rational(0));
        for(int j = 0; j < adj; ++j) {
            int neighbour;
            rational resistance;
            cin >> neighbour >> resistance;
            line[cur] += rational(1)/resistance;
            if (neighbour == IN) continue;
            if (neighbour == OUT) {
                line[n] += PHI/resistance;
                adj_out.push_back(make_pair(cur, resistance));
            }
            else line[neighbour] -= rational(1)/resistance;
        }
        s.addLine(line);
    }
    cout << "Соединены ли исток со стоком?" << endl;
    bool con;
    cin >> con;
    if (con) {
        cout << "Сопротивление?" << endl;
        rational cr;
        cin >> cr;
        adj_out.push_back(make_pair(IN, cr));
    }
    s.Solve();
    auto p = s.getSolution();
    rational I = 0;
    for (auto i : adj_out) {
        if (i.first == IN) {
            I -= PHI / i.second;
            continue;
        }
        I += ((*p)[i.first] - PHI) / i.second;
    }
    rational answer = PHI / I;
    cout << "Сопротивление " << -answer << endl;
    delete p;
}
