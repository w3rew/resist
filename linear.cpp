#include "header.h"
#include <vector>
#include <boost/rational.hpp>
#include <iostream>
using namespace std;
LinearSystem::LinearSystem (int n) 
{
    number = n;
    system.reserve(n);
}
void LinearSystem:: addLine(const vector <rational>& v) {
    system.push_back(v);
}

void LinearSystem::Solve() {


    if (!system.size()) return;
    for (int i = 0; i < number; ++i) {
        for (int j = i; j < number; ++j) {  //находим строку в матрице, в которой в текущем стобце не 0. Ставим ее на первое место 
            if (system[j][i]) {
                swap(system[i], system[j]);
                break;
            }
        }
        for (int j = number; j >= i; --j) { //Нормируем числа в строке, начиная с первого не 0 к первому числу. 
            system[i][j] /= system[i][i];   //Делаем это с конца чтобы не потерять само значение.(против перполнения)
        }
        for (int j = i + 1; j < number; ++j) {
            if (!system[j][i]) continue;
            auto t = system[j][i];
            for (int k = i; k < number + 1; ++k) {
                system[j][k] = system[j][k] * system[i][i] - system[i][k] * t;
            }
        }

    /*for (int i = 0; i < 11; ++i) {
        for (int j = 0; j < 12; ++j) {
            cout << system[i][j] << ' ';
        }
        cout << endl;
    }
    cout << endl << endl;*/
    } //forward

    for (int i = number - 1; i >= 0; --i) {
        system[i][number] /= system[i][i];
        system[i][i] = rational(1);
        for (int j = i - 1; j >= 0; --j) {
            if (!system[j][i]) continue;
            auto t = system[j][i];
            for (int k = 0; k < number + 1; ++k) {
                system[j][k] = system[j][k] * system[i][i] - system[i][k] * t;
            }
        }
    } //backward

    for (int i = 0 ; i < number; ++i) { //Возможно, что оно уже отнормировано, но это не точно
        system[i][number] /= system[i][i];
        system[i][i] = 1;
    }
}
vector<rational>* LinearSystem::getSolution() const {
    if (!system.size()) return 0;
    vector<rational>* res = new vector<rational>(number);
    for (int i = 0; i < number; ++i) {
        (*res)[i] = system[i][number]; //копируем только последний столбец расширенной матрицы
    }
    return res;
}
