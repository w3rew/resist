#ifndef __LINEAR__
#define __LINEAR__
#include <vector>
#include <boost/rational.hpp>
typedef boost::rational<long long> rational;
class LinearSystem {
    public:
        LinearSystem(int n);
        void addLine(const std::vector<rational>&);
        void Solve();
        int size() const {return system.size();}
        std::vector<rational>* getSolution() const;
    private:
        int number;
        std::vector<std::vector<rational>> system;
};
#endif
